//
//  LocationMapView.swift
//  LocationGraph
//
//  Created by Daniil on 15.09.2021.
//

import SwiftUI
import MapKit

struct LocationMapView: View {
    @StateObject var locationManager = LocationManager()
    @State var type: MKMapType = .standard
    var body: some View {
        ZStack{
            let coordinate = self.locationManager.location != nil ? locationManager.location!.coordinate : CLLocationCoordinate2D()
            
            MapView(type: type)
                .environmentObject(locationManager)
                .edgesIgnoringSafeArea(.all)
                .onTapGesture {
                    print("")
                }
            VStack(spacing: 15){
                Text("Longitude - \(coordinate.longitude)")
                Divider()
                Text("Latitude - \(coordinate.latitude)")
            }
            .padding()
            .background(Color.green)
            .cornerRadius(15)
            .font(.system(size: 14, weight: .bold))
            .foregroundColor(Color.black)
            .frame(maxHeight: .infinity, alignment: .top)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding()
            HStack{
                Spacer()
                Button(action: {
                    if type == .standard{
                        type = .hybrid
                    }
                    else if type == .hybrid{
                        type = .hybridFlyover
                    }
                    else if type == .hybridFlyover{
                        type = .mutedStandard
                    }
                    else if type == .mutedStandard{
                        type = .satellite
                    }
                    else if type == .satellite{
                        type = .satelliteFlyover
                    }
                    else if type == .satelliteFlyover{
                        type = .standard
                    }
                    locationManager.map.mapType = type
                    print("\(type)")
                }, label: {
                    Text("Change map type")
                        .foregroundColor(Color.blue)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(8)
                })
            }
            .frame(maxHeight: .infinity, alignment: .bottomTrailing)
            .padding()
        }
        
    }
}



struct MapView: UIViewRepresentable {
    @EnvironmentObject var locationManager: LocationManager
    var type: MKMapType
    func makeUIView(context: Context) -> MKMapView {
        let map = locationManager.map
        map.showsUserLocation = true
        let points = MKMapPoint()
        let pinCoordinate = CLLocationCoordinate2D(latitude: 55.4, longitude: 44.2)
        let pin = MKMapPoint(pinCoordinate)
        points.distance(to: pin)
        map.setUserTrackingMode(.follow, animated: true)
        map.showsCompass = true
        map.mapType = type
        return map
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        
    }
    
    
    
    
    typealias UIViewType = MKMapView
}




struct LocationMapView_Previews: PreviewProvider {
    static var previews: some View {
        LocationMapView()
    }
}


