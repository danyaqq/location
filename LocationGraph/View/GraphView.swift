//
//  GraphView.swift
//  LocationGraph
//
//  Created by Daniil on 15.09.2021.
//

import SwiftUI
import Charts

struct GraphView: View {
    var body: some View {
        VStack{
            MyChartView(chartData: [BarChartDataEntry]())
        }
    }
}


struct ChartModel: Hashable{
    var year: Int
    var number: Int
    func dataEntry(_ year: Int, entry: [ChartModel]) -> [BarChartDataEntry]{
        let year = entry.filter{ $0.year == year}
        return year.map{BarChartDataEntry(x: Double($0.year), y: Double($0.year))}
    }
}

var sampleChartData = [ChartModel(year: 2020, number: 14),ChartModel(year: 2021, number: 44),ChartModel(year: 2022, number: 12)]

struct MyChartView: UIViewRepresentable{
    var chartData: [BarChartDataEntry]
    func makeUIView(context: Context) -> BarChartView {
        let chart = BarChartView()
        chart.barData?.barWidth = 10
    
        return chart
    }
    
    func updateUIView(_ uiView: BarChartView, context: Context) {
        let dataSet = BarChartDataSet(entries: chartData)
        uiView.data = BarChartData(dataSet: dataSet)
    }
    
    typealias UIViewType = BarChartView
}


struct GraphView_Previews: PreviewProvider {
    static var previews: some View {
        GraphView()
    }
}
