//
//  LocationGraphApp.swift
//  LocationGraph
//
//  Created by Daniil on 15.09.2021.
//

import SwiftUI

@main
struct LocationGraphApp: App {
    var body: some Scene {
        WindowGroup {
            LocationMapView()
        }
    }
}
